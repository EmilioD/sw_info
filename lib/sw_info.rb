# frozen_string_literal: true

require 'json'
require 'debug'

# https://ruby-doc.org/core-3.0.3/File.html
# Read JSON file

# https://ruby-doc.org/stdlib-3.0.3/libdoc/json/rdoc/JSON.html
# Parse JSON content to Hash

# https://github.com/ruby/debug

# Cargo toda la data de swapi en la variable JSONDATA como hash

JSONDATA = File.expand_path('swapi.json', __dir__)

DATA = JSON.load_file(JSONDATA)

class Pal
  attr_accessor :data

  def to_s
    @data.map{ |k, v| "#{k}: #{v}"}.join(', ') + "\n"
  end

  def id
    @data['id']
  end

  def name
    @data['name']
  end

  def self.find(id); end

  def self.all; end

  def self.search_for_all(key)
    DATA[key].map { |j| new(j) }
  end

  def self.search_for_id(key, id)
    variable = DATA[key].find { |j| j['id'] == id }
    new(variable)
  end

  def initialize(data)
    @data = data
  end
end

# En esta clase voy a querer acceder a esa data dependiendo de la key que me pase
# y mostrar el atributo que se pida según el método llamado
class Person < Pal
  attr_accessor :data, :alias

  def self.search_for_id(id)
    super('people', id)
  end

  def birth_year
    @data['birth_year']
  end

  def eye_color
    @data['eye_color']
  end

  def hair_color
    @data['hair_color']
  end

  def skin_color
    @data['skin_color']
  end

  def height
    @data['height']
  end

  def mass
    @data['mass']
  end

  def gender
    @data['gender']
  end
end

class Starship < Pal
  attr_accessor :data

  def self.search_for_id(id)
    super('starships', id)
  end 

  def starships
    @data['starships']
  end

  def model
    @data['model']
  end

  def manufacturer
    @data['manufacturer']
  end

  def cost_in_credits
    @data['cost_in_credits']
  end

  def length
    @data['length']
  end

  def max_atmosphering_speed
    @data['max_atmosphering_speed']
  end

  def crew
    @data['crew']
  end

  def passengers
    @data['passengers']
  end

  def cargo_capacity
    @data['cargo_capacity']
  end

  def consumables
    @data['consumables']
  end

  def hyperdrive_rating
    @data['hyperdrive_rating']
  end

  def MGLT
    @data['MGLT']
  end

  def starship_class
    @data['starship_class']
  end
end

class Vehicle < Pal
  attr_accessor :data

  def self.search_for_id(id)
    super('vehicles', id)
  end 

  def vehicles
    @data['vehicles']
  end

  def model
    @data['model']
  end

  def manufacturer
    @data['manufacturer']
  end

  def cost_in_credits
    @data['cost_in_credits']
  end

  def length
    @data['length']
  end

  def max_atmosphering_speed
    @data['max_atmosphering_speed']
  end

  def crew
    @data['crew']
  end

  def passengers
    @data['passengers']
  end

  def cargo_capacity
    @data['cargo_capacity']
  end

  def consumables
    @data['consumables']
  end

  def vehicle_class
    @data['vehicle_class']
  end
end