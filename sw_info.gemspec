  Gem::Specification.new do |s | 
    s.name="sw_info"
    s.version="1.0.9"
    s.summary="Star Wars API"
    s.description="A simple gem that display Star Wars characters info"
    s.authors=["EmilioD"] 
    s.email='emilio@daniel.fake'
    s.files=["Rakefile", "lib/sw_info.rb", "bin/sw_info", "lib/swapi.json"]
    s.homepage='http://rubygems.org/gems/sw_info'
    s.add_dependency("debug")
    s.license='MIT'
    s.required_ruby_version='>=3.0.3'
  end